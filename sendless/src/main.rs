#[macro_use]
extern crate log;
extern crate env_logger;

extern crate batsim;

#[macro_use]
extern crate serde_json;

mod scheduler;

fn main() {
    env_logger::init().unwrap();
    info!("starting up");

    let mut scheduler = scheduler::LazySched::new();
    let mut batsim = batsim::Batsim::new(&mut scheduler);

    batsim.run_simulation().unwrap();
}
