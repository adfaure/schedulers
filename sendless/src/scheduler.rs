extern crate serde_json;
extern crate interval_set;

use self::interval_set::{Interval, IntervalSet, ToIntervalSet};
use batsim::*;

pub struct LazySched {
    running_job: Option<Job>,
    nb_resources: u32,
    resources: IntervalSet,
}

impl LazySched {
    pub fn new() -> LazySched {
        LazySched {
            running_job: None,
            nb_resources: 0,
            resources: IntervalSet::empty(),
        }
    }
}

impl Scheduler for LazySched {
    #[warn(unused_variables)]
    fn simulation_begins(&mut self,
                         timestamp: &f64,
                         nb_resources: i32,
                         _: serde_json::Value)
                         -> Option<Vec<BatsimEvent>> {
        self.nb_resources = nb_resources as u32;
        self.resources = Interval::new(0, self.nb_resources - 1).to_interval_set();
        info!("[{}]FCFS Initialized with {} resources ({})",
              timestamp,
              self.resources.size(),
              self.resources);
        None
    }

    #[warn(unused_variables)]
    fn on_job_submission(&mut self,
                         timestamp: &f64,
                         job: Job,
                         _: Option<Profile>)
                         -> Option<Vec<BatsimEvent>> {

        if job.res as usize > self.nb_resources as usize {
            return Some(vec![reject_job_event(*timestamp, &job)]);
        }

        trace!("Received job {}", job.id);
        let alloc_str = format!("{}-{}", 0, job.res + 10);
        let allocation = allocate_job_event(*timestamp, &job, alloc_str);

        match self.running_job.clone() {
            None => {
                trace!("No job is runnning yet.");
                self.running_job = Some(job.clone());
                Some(vec![allocation])
            }
            Some(ref rjob) => {
                let mut res = vec![];
                let jobs = vec![rjob];

                self.running_job = Some(job.clone());
                trace!("Job {} is running we kill it in order to run job {}",
                       rjob.id,
                       job.id);
                res.push(kill_jobs_event(*timestamp, jobs));
                res.push(allocation);
                Some(res)
            }
        }
    }

    #[warn(unused_variables)]
    fn on_job_completed(&mut self,
                        _: &f64,
                        job_id: String,
                        status: String)
                        -> Option<Vec<BatsimEvent>> {

        trace!("Job={} terminated with Status: {}", job_id, status);
        self.running_job = match self.running_job.clone() {
            Some(cur_job) => {
                if cur_job.id == job_id {
                    None
                } else {
                    Some(cur_job)
                }
            }
            None => None,
        };
        None
    }

    #[warn(unused_variables)]
    fn on_job_killed(&mut self, _: &f64, job_ids: Vec<String>) -> Option<Vec<BatsimEvent>> {
        for job_id in job_ids {
            trace!("Job {} has been killed", job_id);
        }
        None
    }

    fn on_simulation_ends(&mut self, timestamp: &f64) {
        println!("Simulation ends: {}", timestamp);
    }

    fn on_message_received_end(&mut self, timestamp: &mut f64) -> Option<Vec<BatsimEvent>> {
        trace!("Respond to batsim at: {}", timestamp);
        None
    }

    fn on_message_received_begin(&mut self, timestamp: &f64) -> Option<Vec<BatsimEvent>> {
        trace!("Received new batsim message at {}", timestamp);
        None
    }
}
