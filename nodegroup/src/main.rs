#[macro_use]
extern crate log;
extern crate env_logger;
extern crate batsim;
extern crate interval_set;

#[macro_use]
extern crate serde_json;
extern crate getopts;

mod grp_scheduler;
mod rej_grp;
mod fcfs;
mod generic;


fn main() {

    env_logger::init().unwrap();
    let mut scheduler = grp_scheduler::MainScheduler::new();
    let mut batsim = batsim::Batsim::new(&mut scheduler);

    batsim.run_simulation().unwrap();
}
