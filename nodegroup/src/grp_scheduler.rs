extern crate serde_json;
extern crate interval_set;
extern crate regex;

use batsim::*;
use generic::*;
use rej_grp::*;
use fcfs::*;

use interval_set::{ToIntervalSet, Interval};
use std::collections::HashMap;
use std::rc::Rc;
use std::ops::DerefMut;

use std::cell::Cell;
use std::cell::RefCell;
use std::str::FromStr;


#[derive(Debug)]
pub struct RejectedJob {
    job_id: String,
    initial_job: Rc<Job>,
    resub_job: Rc<Job>,
    ack_received: Cell<bool>,
    finished: Cell<bool>,
}

#[derive(Debug)]
pub struct Stat {
    total_rej: i64,
    total_jobs: i64,
    not_scheduled: i64,
    total_finished: i64,
}

impl Stat {
    pub fn new() -> Stat {
        Stat {
            total_rej: 0,
            total_jobs: 0,
            total_finished: 0,
            not_scheduled: 0,
        }
    }
}

/// This class is a meta scheduler which handles a subset of resources
/// nb_resources: The total number of avalable resources
/// time: the current time of the simulation
/// schdedulers: The set of subscheduler
/// the config provided by batsim
/// the max job size the cluster can handle
/// jobs: A HashMap of all jobs
/// rejected_job: A hashmap of job that have been rejected and must be replay
/// profiles: A HashMap of all profiles used by job
/// rej_scheduler: The scheduler that will handle the rejected job
/// waiting_jobs: We keep track of special jobs that need a big part of the cluster
/// and needs a special treatment
/// nb_dangling_resources: Number o resources that are not use in the subschedulers
/// stats: An object that keep track of the simulation
pub struct MainScheduler {
    nb_resources: u32,
    time: f64,
    schedulers: Vec<Box<GrpScheduler>>,
    config: serde_json::Value,
    max_job_size: usize,
    jobs: HashMap<String, Rc<Job>>,
    rejected_jobs: HashMap<String, Rc<RejectedJob>>,
    profiles: HashMap<String, Profile>,
    rej_sched: Option<Box<GrpScheduler>>,
    waiting_jobs: HashMap<String, RefCell<Allocation>>,
    nb_dangling_resources: i32,
    pub stats: Stat,
    pub threshold: i32,
}

impl MainScheduler {
    pub fn new() -> MainScheduler {
        info!("Init empty MainScheduler Scheduler");
        MainScheduler {
            nb_resources: 0,
            max_job_size: 0,
            time: 0.0,
            schedulers: vec![],
            config: json!(null),
            jobs: HashMap::new(),
            waiting_jobs: HashMap::new(),
            rejected_jobs: HashMap::new(),
            profiles: HashMap::new(),
            rej_sched: None,
            nb_dangling_resources: 0,
            threshold: 0,
            stats: Stat::new(),
        }
    }

    fn schedule_jobs(&mut self) -> Option<Vec<BatsimEvent>> {
        let mut allocation_array: Vec<Allocation> = vec![];
        let mut events: Vec<BatsimEvent> = vec![];

        let dan_size = self.nb_dangling_resources;
        // We reject only jobs that can be run to the
        // remaining space.
        let cb = |j: &Job| j.res <= dan_size && !j.id.starts_with("temp!");

        // We ask each scheduler what to do
        for grp in self.schedulers.iter_mut() {
            // The scheduler returns events
            // And can reject one of its job
            let (allocs, rejected) = grp.schedule_jobs(self.time, &cb);

            match allocs {
                None => {}
                Some(mut vec) => {
                    allocation_array.append(&mut vec);
                }
            }

            match rejected {
                None => {}
                Some(job) => {
                    let (_, job_id) = Job::split_id(&job.id);
                    let id = format!("rej!{}", job_id);

                    match self.rejected_jobs.get(&id) {
                        None => {
                            events.push(kill_jobs_event(self.time, vec![&job]));
                            let killed_job = MainScheduler::construct_rejected_job(job);
                            trace!("INSERT Job {:?} into rejected", id);
                            self.rejected_jobs.insert(id, Rc::new(killed_job));
                        }
                        _ => {
                            trace!("Job {:?} has already been rejected", job);
                        }
                    }

                }
            };
        }

        let mut mut_events: Vec<Allocation> = vec![];
        // We use a dedicated scheduler for the rejected job
        match self.rej_sched {
            Some(ref mut sched) => {
                let (events, rejected) = sched.schedule_jobs(self.time, &|_: &Job| false);
                match events {
                    None => {}
                    Some(mut vec) => {
                        mut_events.append(&mut vec);
                    }
                }

                match rejected {
                    None => {}
                    Some(_) => {
                        panic!("This scheduler is not allowed to reject job");
                    }
                }
            }
            None => {
                trace!("No sched");
            }
        }

        allocation_array.extend(mut_events);

        // To deal with huge jobs that require this part of the cluser
        // I hack a litle bit my scheduler. So we have to reconstruct
        // the allocation before we send it to batsim.
        let big_jobs: Vec<Allocation> = allocation_array
            .into_iter()
            .map(|mut alloc| {
                     let (w_id, j_id) = Job::split_id(&alloc.job.id);
                     if w_id == "temp!" {
                         // We reconstruct the job so batsim can be consistent
                         alloc.job = self.jobs.get(&j_id).unwrap().clone();
                     }
                     alloc
                 })
            .collect();

        // At this point we need to find out jobs that are ready to be executed
        let (mut jobs_ready, jobs_uncomplete): (Vec<Allocation>, Vec<Allocation>) =
            big_jobs
                .into_iter()
                .partition(|alloc| alloc.job.res <= alloc.nodes.size() as i32);

        // If we get uncomplete jobs, we update the new allocations
        // if this is the first time we encounter the job
        // we register it otherwse we aggregate resources
        for mut alloc in jobs_uncomplete {
            if self.waiting_jobs.contains_key(&alloc.job.id) {
                let cell_alloc = self.waiting_jobs.get_mut(&alloc.job.id).unwrap();
                trace!("Getting more allocation for huge job {:?} {:?}",
                       alloc.job,
                       alloc.nodes);
                alloc.nodes = alloc.nodes.union(cell_alloc.borrow().nodes.clone());
                *cell_alloc.borrow_mut() = alloc;
            } else {
                trace!("New huge job {:?} {:?}", alloc.job, alloc.nodes);
                self.waiting_jobs
                    .insert(alloc.job.id.clone(), RefCell::new(alloc));
            }
        }


        jobs_ready.extend(self.update_waiting_jobs());

        events.extend(MainScheduler::allocations_to_batsim_events(self.time, jobs_ready));
        Some(events)
    }

    fn update_waiting_jobs(&mut self) -> Vec<Allocation> {
        let completed_jobs: Vec<Allocation> = self.waiting_jobs
            .iter()
            .filter_map(|refc_alloc| {
                            let alloc = refc_alloc.1.borrow();
                            if alloc.nodes.size() >= alloc.job.res as u32 {
                                Some(alloc.clone())
                            } else {
                                None
                            }
                        })
            .collect();

        // I clear the jobs completed, maybe there is another way to do that
        for alloc in completed_jobs.iter() {
            trace!("We found one allocation for {:?}", alloc.job);
            self.waiting_jobs.remove(&alloc.job.id);
        }

        completed_jobs
    }

    // Insert a job to th right sub scheduler
    fn add_job(&mut self, job: Rc<Job>) {
        let grp = get_job_grp(&*job);

        // This is the simpler case
        // we handle a job that will run into
        // one of the subscheulers
        match self.schedulers.get_mut(grp) {
            Some(sched) => {
                sched.deref_mut().add_job(job.clone());
                self.jobs.insert(job.id.clone(), job.clone());
                return;
            }
            _ => {}
        };


        // If the jobs do not fit into any of the sub schedulers
        // we need to split to send it to all sub schedulers till
        // we have enought cores.
        let mut iter_sched = self.schedulers.iter_mut().enumerate().rev();
        let mut sched = iter_sched.next().unwrap();
        let mut cores_remaining = job.res;

        trace!("New huge job has been added {:?}", job);
        while cores_remaining > 0 && sched.0 > 0 {
            let mut complete_job = job.as_ref().clone();
            // We create a temporary job, and we tag it with a
            // scpecial id. So we can retrieve it a the schedule time
            complete_job.id = format!("temp!{}", job.id.clone());
            complete_job.res = cores_remaining;

            sched.1.add_job(Rc::new(complete_job.clone()));

            cores_remaining -= 2i32.pow(sched.0 as u32);
            //info!("Submit to: {:?} use {:?} nodes. {} remains", sched.0, 2i32.pow(sched.0 as u32), cores_remaining);
            sched = iter_sched.next().unwrap();
        }

        // This is the special case were the job requires also nodes
        // from the rejected scheduler.
        if cores_remaining > 0 {
            trace!("We even need the rej sched ");
            match self.rej_sched {
                Some(ref mut sched) => {
                    let mut complete_job = job.as_ref().clone();
                    // We create a temporary job, and we tag it with a
                    // scpecial id. So we can find it a the schedule time
                    complete_job.id = format!("temp!{}", job.id.clone());
                    complete_job.res = cores_remaining;
                    sched.add_job(Rc::new(complete_job));
                }
                None => panic!("This should never happends, sorry ... "),
            }
        }

        self.jobs.insert(job.id.clone(), job.clone());
    }

    // Called when a job is finished
    fn free_job(&mut self, job_id: &String) {
        let job: Rc<Job> = (self.jobs.get(job_id).unwrap()).clone();
        let grp = get_job_grp(job.as_ref());

        match self.schedulers.get_mut(grp) {
            Some(sched) => {
                sched.job_completed(job.clone());
                return;
            }
            None => {}
        };

        let mut iter_sched = self.schedulers.iter_mut().enumerate().rev();
        let mut sched = iter_sched.next().unwrap();
        let mut cores_remaining = job.res;

        let mut complete_job = job.as_ref().clone();
        complete_job.id = format!("temp!{}", job.id.clone());
        complete_job.res = cores_remaining;

        let shared_job = Rc::new(complete_job);

        while cores_remaining > 0 && sched.0 > 0 {
            trace!("Huge job has been finished {:?}", job_id);
            sched.1.job_completed(shared_job.clone());
            cores_remaining -= 2i32.pow(sched.0 as u32);
            sched = iter_sched.next().unwrap();
        }

        if cores_remaining > 0 {
            match self.rej_sched {
                Some(ref mut sched) => {
                    sched.job_completed(shared_job);
                }
                None => panic!("This should never happends, sorry ... "),
            }
        }
    }

    // Received when a job as been killed
    // we wait for the message from batsim
    // before resubmitted the rejected_job
    fn killed_job(&mut self, job_id: &String) {
        let job: Rc<Job> = (self.jobs.get(job_id).unwrap()).clone();
        let grp = get_job_grp(job.as_ref());

        match self.schedulers.get_mut(grp) {
            Some(sched) => {
                sched.job_killed(job.clone());
            }
            None => panic!("No sched can handle this kind of job"),
        };
    }

    fn construct_rejected_job(job: Rc<Job>) -> RejectedJob {
        let (_, job_id) = Job::split_id(&job.id);

        let mut resub_job = (*job).clone();
        resub_job.id = format!("rej!{}", job_id);
        RejectedJob {
            job_id: resub_job.id.clone(),
            initial_job: job.clone(),
            resub_job: Rc::new(resub_job),
            ack_received: Cell::new(false),
            finished: Cell::new(false),
        }
    }

    fn allocations_to_batsim_events(now: f64, allocation: Vec<Allocation>) -> Vec<BatsimEvent> {

        allocation
            .into_iter()
            .filter_map(|alloc| {
                //                let job = self.jobs.get(alloc.job.id);
                if alloc.job.res as u32 <= alloc.nodes.size() {
                    return Some(allocate_job_event(now, &*alloc.job, format!("{}", alloc.nodes)));
                }
                None
            })
            .collect()
    }
}

impl Scheduler for MainScheduler {
    fn simulation_begins(&mut self,
                         timestamp: &f64,
                         nb_resources: i32,
                         config: serde_json::Value)
                         -> Option<Vec<BatsimEvent>> {

        info!("MainScheduler Initialized with {} resources", nb_resources);
        self.nb_resources = nb_resources as u32;
        let nb_grps: u32 = (nb_resources as f64).log(2_f64).floor() as u32;
        // self.max_job_size = (2 as u32).pow(nb_grps - 1) as usize;
        self.max_job_size = self.nb_resources as usize;

        info!("We can construct {} groups with {} resources",
              nb_grps,
              nb_resources);
        info!("The max job size accepted is {}", self.max_job_size);

        self.config = config;
        // We get the threshold from the configuration
        let threshold: i32 = i32::from_str(&self.config["rejection"]["threshold"].to_string())
            .unwrap();
        self.threshold = threshold;
        info!("Threshold is set at: {}", threshold);

        let mut nb_machines_used = 0;
        for idx in 0..nb_grps {
            let mut scheduler: RejGrpScheduler = RejGrpScheduler::new();
            let interval = Interval::new(2_u32.pow(idx) as u32, (2_u32.pow(idx + 1) - 1) as u32);
            nb_machines_used += interval.range_size();
            info!("Groupe(n={}) uses {} nodes", idx, interval.range_size());
            scheduler.initialize(idx as i32, vec![interval].to_interval_set());

            let mut sch = Box::new(scheduler);
            sch.set_threshold(self.threshold);
            self.schedulers.insert(idx as usize, sch);
        }

        info!("We use only {} over {}",
              nb_machines_used,
              self.nb_resources - 1);

        let mut rejsched = Box::new(FCFS::new());

        self.nb_dangling_resources = (self.nb_resources - 1 - nb_machines_used) as i32;
        if self.nb_dangling_resources <= 0 {
            panic!("No ressource available for rejection");
        }
        rejsched.initialize(0, /* osef ?*/
                            vec![(nb_machines_used + 1, self.nb_resources - 1)].to_interval_set());

        self.rej_sched = Some(rejsched);
        self.nb_dangling_resources = (self.nb_resources - 1 - nb_machines_used) as i32;

        info!("Job rejection maximun size is {}",
              self.nb_dangling_resources);
        // We tell to batsim that it doesnt have to wait for us.
        Some(vec![notify_event(*timestamp, String::from("submission_finished"))])
    }

    fn on_job_submission(&mut self,
                         timestamp: &f64,
                         job: Job,
                         profile: Option<Profile>)
                         -> Option<Vec<BatsimEvent>> {

        self.time = *timestamp;

        if job.res as usize > self.max_job_size {
            self.stats.not_scheduled += 1;
            return Some(vec![reject_job_event(self.time, &job)]);
        }

        match profile {
            Some(p) => {
                self.profiles.insert(job.profile.clone(), p);
            }
            None => panic!("Did you forget to activate the profile forwarding ?"),
        }

        let (w_id, j_id) = Job::split_id(&job.id);
        match w_id.as_ref() {
            "rej!" => {
                trace!("REJECTED job(={:?}) with size {}", job, job.res);
                let rej_id = format!("rej!{}", j_id);
                let rejj = self.rejected_jobs.get(&rej_id).unwrap().clone();
                match self.rej_sched {
                    Some(ref mut sched) => {
                        sched.add_job(rejj.resub_job.clone());
                    }
                    None => panic!("Impossible to handle rejected job: {:?}", rejj),
                }
            }
            _ => {
                trace!("Get a new job(={:?}) with size {}", job, job.res);
                let grp_idx = get_job_grp(&job);
                let job_rc = Rc::new(job);

                self.add_job(job_rc.clone());
                trace!("The job is redirectd to group {}", grp_idx);
            }
        }
        None
    }

    fn on_job_completed(&mut self,
                        timestamp: &f64,
                        job_id: String,
                        _: String)
                        -> Option<Vec<BatsimEvent>> {

        let (w_id, j_id) = Job::split_id(&job_id);

        self.stats.total_jobs += 1;
        match w_id.as_ref() {
            "rej!" => {
                self.stats.total_rej += 1;
                trace!("REJECTED job(={:?}) finished", job_id);
                let rej_id = format!("rej!{}", j_id);
                let job = self.rejected_jobs
                    .get(&rej_id)
                    .unwrap()
                    .resub_job
                    .clone();
                match self.rej_sched {
                    Some(ref mut sched) => {
                        sched.job_completed(job);
                    }
                    None => {}
                }
                None
            }
            _ => {

                self.stats.total_finished += 1;
                trace!("Job completed {}", job_id);
                // In some cases, the job may finish before the kill, in this situation batsim sends
                // both ack, so we record that a job finished so we wont re submit it.
                if let Some(rejj) = self.rejected_jobs.get(&Job::split_id(&job_id).1) {
                    rejj.finished.set(true);
                }

                self.time = *timestamp;
                self.free_job(&job_id);
                None
            }
        }
    }

    fn on_simulation_ends(&mut self, timestamp: &f64) {
        info!("Simulation ends: {}", timestamp);
        info!("Stats = {:?}", self.stats);
    }

    fn on_job_killed(&mut self, timestamp: &f64, job_ids: Vec<String>) -> Option<Vec<BatsimEvent>> {

        let mut res = vec![];
        for job_id in job_ids {

            trace!("Recover killed  job {:?}", job_id);
            let rej_id = format!("rej!{}", Job::split_id(&job_id).1);

            let rejj = self.rejected_jobs.get(&rej_id).unwrap().clone();

            if rejj.ack_received.get() | rejj.finished.get() {
                trace!("Job already done or already ack (ack: {}, done: {})",
                       rejj.ack_received.get(),
                       rejj.finished.get());
                continue;
            }

            trace!("Killed  job {:?}", rejj);
            rejj.ack_received.set(true);
            self.killed_job(&rejj.initial_job.id);

            let profile: Option<&Profile> = self.profiles.get(&rejj.initial_job.profile);
            let event = submit_job_event(*timestamp, &rejj.resub_job, profile);
            res.push(event);
        }

        if res.len() > 0 { Some(res) } else { None }
    }

    fn on_message_received_end(&mut self, _: &mut f64) -> Option<Vec<BatsimEvent>> {
        let events = self.schedule_jobs();
        events
    }

    fn on_message_received_begin(&mut self, _: &f64) -> Option<Vec<BatsimEvent>> {
        None
    }
}

fn get_job_grp(job: &Job) -> usize {
    (job.res as f64).log(2_f64).ceil() as usize
}
