extern crate serde_json;
extern crate interval_set;

use std::rc::Rc;
use self::interval_set::IntervalSet;
use batsim::*;



//Representing a job and it allocated node by the
//generic scheduler.
#[derive(Debug, Clone)]
pub struct Allocation {
    pub nodes: IntervalSet,
    pub job: Rc<Job>
}

pub trait GrpScheduler {
    fn initialize(&mut self, idx: i32, resources: IntervalSet);

    fn set_threshold(&mut self, threshold: i32);
    fn compute_load(&self) -> i32 {
        0
    }

    /// `schedule_jobs` can also choose to reject a job,
    /// if so it return a ref to the job in the tuple.
    fn schedule_jobs(&mut self,
                     time: f64,
                     is_rejection_possible: &Fn(&Job) -> bool)
                     -> (Option<Vec<Allocation>>, Option<Rc<Job>>);

    fn is_job_elligible(&mut self) -> bool {
        false
    }
    fn add_job(&mut self, job: Rc<Job>);
    fn job_completed(&mut self, job: Rc<Job>);
    fn job_killed(&mut self, job: Rc<Job>);
    fn get_running_jobs(&self) -> Option<Rc<Job>>;
}
