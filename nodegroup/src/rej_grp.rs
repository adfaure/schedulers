extern crate serde_json;
extern crate interval_set;

use std::cmp;
use batsim::*;
use std::collections::LinkedList;
use std::rc::Rc;
use interval_set::{IntervalSet, ToIntervalSet};

use generic::*;

pub struct RejGrpScheduler {
    running_job: Option<Rc<Job>>,
    job_queue: LinkedList<Rc<Job>>,
    resources: IntervalSet,
    grp_idx: i32,
    id_first_node: i32,
    kill_pending: Option<Rc<Job>>,
    threshold: i32,
}

impl RejGrpScheduler {
    pub fn new() -> RejGrpScheduler {
        RejGrpScheduler {
            running_job: None,
            kill_pending: None,
            job_queue: LinkedList::new(),
            resources: IntervalSet::empty(),
            grp_idx: 0,
            id_first_node: 0,
            threshold: i32::max_value(),
        }
    }
}

impl GrpScheduler for RejGrpScheduler {
    fn compute_load(&self) -> i32 {
        self.job_queue.len() as i32
    }

    fn set_threshold(&mut self, threshold: i32) {
        self.threshold = threshold;
    }

    fn initialize(&mut self, idx: i32, resources: IntervalSet) {
        self.resources = resources;
        self.grp_idx = idx;
        self.id_first_node = self.resources.iter().min().unwrap().get_inf() as i32;
        info!("Init groupe {} to {}", self.grp_idx, self.resources);
    }

    fn add_job(&mut self, job: Rc<Job>) {
        self.job_queue.push_back(job.clone());
    }

    fn job_completed(&mut self, job: Rc<Job>) {

        match &self.kill_pending {
            &None => { /* We just ignore it */ }
            &Some(ref k_job) => {
                if job.id == k_job.id {
                    return;
                } else {
                    panic!("We received a job finished for job {:?} \
                           when we wait for this job to be killed {}",
                           job,
                           k_job)
                }
            }
        }

        self.running_job = match self.running_job.clone() {
            Some(r_job) => {
                if r_job.id == job.id {
                    None
                } else {
                    panic!("It seems to have a sync problem {} \
                           should be runnning while {} is completed",
                           r_job.id,
                           job.id)
                }
            }
            None => {
                panic!("It seems to have a sync problem,\
                       no job is running and {} is decalred finished",
                       job.id)
            }
        };

        trace!("{:?} currently running, queue size: {}",
               self.running_job,
               self.job_queue.len());
    }

    fn job_killed(&mut self, job: Rc<Job>) {
        self.kill_pending = match self.kill_pending.clone() {
            None => panic!("No job has been killed by this scheduler ... {:?}", job),
            Some(k_job) => {
                if job.id == k_job.id {
                    trace!("Job killed correctly {:?}", job);
                    None
                } else {
                    panic!("A job has been kill but not the one we expected...");
                }
            }
        }
    }

    fn schedule_jobs(&mut self,
                     _: f64,
                     is_rejection_possible: &Fn(&Job) -> bool)
                     -> (Option<Vec<Allocation>>, Option<Rc<Job>>) {

        match &self.kill_pending {
            &Some(ref kjob) => {
                trace!("No schdedule to do, we are waiting for a kill ack of job {}",
                       kjob.id);
                return (None, None);
            }
            _ => {}
        }

        match self.running_job.clone() {
            None => {
                trace!("{:?} currently running, queue size: {} on grp: {}",
                       self.running_job,
                       self.job_queue.len(),
                       self.grp_idx);

                let job = match self.job_queue.pop_front() {
                    None => return (None, None),
                    Some(job) => job,
                };

                // In some case a job might be to big for the group
                // or to small so we take the min
                let last_node = cmp::min( (self.id_first_node + (job.res) - 1), self.id_first_node + 2i32.pow(self.grp_idx as u32) - 1);

                trace!("res: {} laste node: {}  {}", job.res, (self.id_first_node + (job.res) - 1), last_node);
                let alloc = vec![(self.id_first_node as u32, last_node as u32)]
                    .to_interval_set();

                self.running_job = Some(job.clone());
                (Some(vec![Allocation {
                               nodes: alloc,
                               job: job.clone(),
                           }]),
                 None)
            }
            Some(job) => {
                trace!("{:?} currently running, queue size: {} grp: {}",
                       self.running_job,
                       self.job_queue.len(),
                       self.grp_idx);

                let current_load = self.compute_load();
                // we check if the kill is not already pending
                // if the rejection is possible and if the load
                // rech its threshold
                if current_load > self.threshold && is_rejection_possible(&job) {
                    trace!("Loa is: {} threshold set at {}",
                           current_load,
                           self.threshold);
                    trace!("Load reach its treshold {} killing job {:?}",
                           current_load,
                           job);
                    self.kill_pending = Some(job.clone());
                    self.running_job = None;
                    return (None, Some(job.clone()));
                }
                (None, None)
            }
        }

    }

    fn get_running_jobs(&self) -> Option<Rc<Job>> {
        self.running_job.clone()
    }
}
