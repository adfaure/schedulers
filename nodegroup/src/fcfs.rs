extern crate serde_json;
extern crate interval_set;

use self::interval_set::{Interval, IntervalSet};
use batsim::*;
use std::collections::LinkedList;
use std::collections::HashMap;

use std::rc::Rc;

use generic::*;

pub struct FCFS {
    pub nb_resources: u32,
    pub grp_idx: i32,
    pub resources: IntervalSet,
    pub running_jobs: HashMap<String, (Rc<Job>, IntervalSet)>,
    pub job_queue: LinkedList<Rc<Job>>,
}

impl FCFS {
    pub fn new() -> FCFS {
        info!("Init empty FCFS Scheduler");
        FCFS {
            nb_resources: 0,
            resources: IntervalSet::empty(),
            running_jobs: HashMap::new(),
            grp_idx: 0,
            job_queue: LinkedList::new(),
        }
    }

    fn job_finished_internal(&mut self, job_id: &String) {
        let (_, allocation) = self.running_jobs.remove(job_id).unwrap();
        self.resources = self.resources.clone().union(allocation);
    }

    fn launch_job_internal(&mut self, job: Rc<Job>, allocation: IntervalSet) {
        trace!("Launching Job::Job={} allocation={}", job.id, allocation);

        self.resources = self.resources.clone().difference(allocation.clone());
        self.running_jobs
            .insert(job.id.clone(), (job.clone(), allocation));
    }

    fn find_job_allocation(&self, job: &Job) -> Option<IntervalSet> {
        let current_available_size = self.resources.size();
        if current_available_size < (job.res as u32) {
            trace!("No allocation possible yet for the job {} (nb res={}) \
                   (size available={})",
                   job.id,
                   job.res,
                   current_available_size);
            return None;
        }

        trace!("Try to allocate Job={} res={}", job.id, job.res);

        let mut iter = self.resources.iter();
        let mut allocation = IntervalSet::empty();
        let mut left = job.res as u32;

        let mut interval = iter.next().unwrap();
        while allocation.size() != (job.res as u32) {
            // Note that we test earlier in the function if the interval
            // has enought resources, so this loop should not fail.
            let interval_size = interval.range_size();

            if interval_size > left {
                allocation.insert(Interval::new(interval.get_inf(), interval.get_inf() + left - 1));
            } else if interval_size == left {
                allocation.insert(interval.clone());
            } else if interval_size < left {
                allocation.insert(interval.clone());
                left -= interval_size;
                interval = iter.next().unwrap();
            }
        }

        Some(allocation)
    }
}

impl GrpScheduler for FCFS {
    fn initialize(&mut self, _: i32, resources: IntervalSet) {
        self.nb_resources = resources.size() as u32;
        self.resources = resources;

        info!("FCFS Initialized with {} resources ({})",
              self.resources.size(),
              self.resources);

    }

    fn set_threshold(&mut self, _: i32) {
        panic!("This function is not supported by this scheduler")
    }

    fn add_job(&mut self, job: Rc<Job>) {
        self.job_queue.push_back(job);
    }

    fn job_completed(&mut self, job: Rc<Job>) {
        trace!("Job={} terminated ", job.id);
        trace!("{} currently running", self.running_jobs.len());
        trace!("{} waiting on the queue", self.job_queue.len());
        self.job_finished_internal(&job.id);
        trace!("{} currently running", self.running_jobs.len());
        trace!("{} waiting on the queue", self.job_queue.len());
    }

    fn job_killed(&mut self, _: Rc<Job>) {
        panic!("This function should not be called on this scheduler");
    }

    fn schedule_jobs(&mut self,
                     _: f64,
                     _: &Fn(&Job) -> bool)
                     -> (Option<Vec<Allocation>>, Option<Rc<Job>>) {

        let mut res: Vec<Allocation> = Vec::new();
        let mut optional = self.job_queue.pop_front();
        while let Some(job) = optional {
            match self.find_job_allocation(&job) {
                None => {
                    trace!("Cannot laucnh job={} now", job.id);
                    self.job_queue.push_front(job);
                    optional = None;
                }
                Some(allocation) => {
                    res.push(Allocation{
                        nodes: allocation.clone(),
                        job: job.clone()
                    });
                    self.launch_job_internal(job.clone(), allocation.clone());
                    optional = self.job_queue.pop_front();
                }
            }
        }
        (Some(res), None)
    }

    fn get_running_jobs(&self) -> Option<Rc<Job>> {
        panic!("If needed please update GrpScheduler to allow to return several jobs");
    }
}
