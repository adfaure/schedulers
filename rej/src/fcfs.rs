extern crate serde_json;
extern crate interval_set;

use batsim::*;

pub struct Rej;

impl Rej {
    pub fn new() -> Rej {
        info!("Init empty Rej Scheduler");
        Rej {}
    }
}

impl Scheduler for Rej {
    fn simulation_begins(&mut self,
                         _: &f64,
                         _: i32,
                         _: serde_json::Value)
                         -> Option<Vec<BatsimEvent>> {
        None
    }

    fn on_job_submission(&mut self,
                         timestamp: &f64,
                         job: Job,
                         _: Option<Profile>)
                         -> Option<Vec<BatsimEvent>> {
        Some(vec![reject_job_event(*timestamp, &job)])
    }
    fn on_job_completed(&mut self, _: &f64, _: String, _: String) -> Option<Vec<BatsimEvent>> {
        None
    }
    fn on_simulation_ends(&mut self, timestamp: &f64) {
        println!("Simulation ends: {}", timestamp);
    }

    fn on_job_killed(&mut self, _: &f64, _: Vec<String>) -> Option<Vec<BatsimEvent>> {
        None
    }

    fn on_message_received_end(&mut self, timestamp: &mut f64) -> Option<Vec<BatsimEvent>> {
        trace!("Respond to batsim at: {}", timestamp);
        None
    }

    fn on_message_received_begin(&mut self, timestamp: &f64) -> Option<Vec<BatsimEvent>> {
        trace!("Received new batsim message at {}", timestamp);
        None
    }
}
