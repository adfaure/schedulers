extern crate serde_json;
extern crate interval_set;

use self::interval_set::{Interval, IntervalSet, ToIntervalSet};
use batsim::*;
use std::collections::LinkedList;
use std::collections::HashMap;

pub struct FCFS {
    pub nb_resources: u32,
    pub resources: IntervalSet,
    pub time: f64,
    pub running_jobs: HashMap<String, (Job, IntervalSet)>,
    pub job_queue: LinkedList<Job>,
    pub config: serde_json::Value,
}

impl FCFS {
    pub fn new() -> FCFS {
        info!("Init empty FCFS Scheduler");
        FCFS {
            nb_resources: 0,
            time: 0.0,
            resources: IntervalSet::empty(),
            running_jobs: HashMap::new(),
            job_queue: LinkedList::new(),
            config: json!(null),
        }
    }

    fn schedule_job(&mut self, _: f64) -> Option<Vec<BatsimEvent>> {
        let mut res: Vec<BatsimEvent> = Vec::new();
        let mut optional = self.job_queue.pop_front();
        while let Some(job) = optional {
            match self.find_job_allocation(&job) {
                None => {
                    trace!("Cannot laucnh job={} now", job.id);
                    self.job_queue.push_front(job);
                    optional = None;
                }
                Some(allocation) => {
                    let alloc_str = format!("{}", allocation);
                    res.push(allocate_job_event(self.time, &job, alloc_str));
                    self.launch_job_internal(job.clone(), allocation.clone());
                    optional = self.job_queue.pop_front();
                }
            }
        }
        Some(res)
    }

    fn job_finished_internal(&mut self, job_id: String) {
        let (_, allocation) = self.running_jobs.remove(&job_id).unwrap();
        self.resources = self.resources.clone().union(allocation);
    }

    fn launch_job_internal(&mut self, job: Job, allocation: IntervalSet) {
        trace!("Launching Job::Job={} allocation={}", job.id, allocation);

        self.resources = self.resources.clone().difference(allocation.clone());
        self.running_jobs
            .insert(job.id.clone(), (job, allocation));
    }

    fn find_job_allocation(&self, job: &Job) -> Option<IntervalSet> {
        let current_available_size = self.resources.size();
        if current_available_size < (job.res as u32) {
            trace!("No allocation possible yet for the job {} (nb res={}) (size available={})",
                   job.id,
                   job.res,
                   current_available_size);
            return None;
        }

        trace!("Try to allocate Job={} res={}", job.id, job.res);

        let mut iter = self.resources.iter();
        let mut allocation = IntervalSet::empty();
        let mut left = job.res as u32;

        let mut interval = iter.next().unwrap();
        while allocation.size() != (job.res as u32) {
            // Note that we test earlier in the function if the interval
            // has enought resources, so this loop should not fail.
            let interval_size = interval.range_size();

            if interval_size > left {
                allocation.insert(Interval::new(interval.get_inf(), interval.get_inf() + left - 1));
            } else if interval_size == left {
                allocation.insert(interval.clone());
            } else if interval_size < left {
                allocation.insert(interval.clone());
                left -= interval_size;
                interval = iter.next().unwrap();
            }
        }

        Some(allocation)
    }
}

impl Scheduler for FCFS {
    fn simulation_begins(&mut self,
                         timestamp: &f64,
                         nb_resources: i32,
                         config: serde_json::Value)
                         -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;
        self.config = config;
        self.nb_resources = nb_resources as u32;
        self.resources = Interval::new(0, self.nb_resources - 1).to_interval_set();
        info!("FCFS Initialized with {} resources ({})",
              self.resources.size(),
              self.resources);
        None
    }

    fn on_job_submission(&mut self,
                         timestamp: &f64,
                         job: Job,
                         profile: Option<Profile>)
                         -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;

        if job.res as usize > self.nb_resources as usize {
            return Some(vec![reject_job_event(self.time, &job)]);
        }

        self.job_queue.push_back(job);
        None
        //self.schedule_job(*timestamp)
    }
    fn on_job_completed(&mut self,
                        timestamp: &f64,
                        job_id: String,
                        status: String)
                        -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;
        trace!("Job={} terminated with Status: {}", job_id, status);
        self.job_finished_internal(job_id);
        None
        //self.schedule_job(*timestamp)
    }
    fn on_simulation_ends(&mut self, timestamp: &f64) {
        self.time = *timestamp;
        println!("Simulation ends: {}", timestamp);
    }

    fn on_job_killed(&mut self, timestamp: &f64, job_ids: Vec<String>) -> Option<Vec<BatsimEvent>> {
        for job_id in job_ids {
            trace!("Job {} has been killed {}", job_id, timestamp);
        }
        None
    }

    fn on_message_received_end(&mut self, timestamp: &mut f64) -> Option<Vec<BatsimEvent>> {
        trace!("Respond to batsim at: {}", timestamp);
        self.schedule_job(*timestamp)
    }

    fn on_message_received_begin(&mut self, timestamp: &f64) -> Option<Vec<BatsimEvent>> {
        trace!("Received new batsim message at {}", timestamp);
        None
    }
}
