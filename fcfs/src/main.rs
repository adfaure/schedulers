#[macro_use]
extern crate log;
extern crate env_logger;

extern crate batsim;

#[macro_use]
extern crate serde_json;

mod fcfs;

fn main() {
    env_logger::init().unwrap();
    info!("starting up");

    let mut scheduler = fcfs::FCFS::new();
    let mut batsim = batsim::Batsim::new(&mut scheduler);

    batsim.run_simulation().unwrap();
}
